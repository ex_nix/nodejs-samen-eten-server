#+TITLE: TODO list

* TODO
** Must have
*** DONE UC-101 - Register users
**** DONE Verplicht veld ontbreekt
**** DONE Invalide emailadres
**** DONE invalide wachtwoord
**** DONE Gebruiker bestaat al
**** DONE Gebruiker succesvol geregistreerd
*** DONE UC-102 - Login users
**** DONE Verplicht veld ontbreekt
**** DONE Invalide emailadres
**** DONE invalide wachtwoord
**** DONE Gebruiker bestaat niet
**** DONE Gebruiker succesvol ingelogd
*** DONE UC-103 - /api/info

*** DONE UC-201 - Create student home
**** DONE Gebruiker heeft geen toegang
**** DONE Eén of meer ingevoerde velden ontbreken
**** DONE Er bestaat al een huis op het adres
*** DONE UC-202 - Get list of student homes (empty list = code 200)
**** DONE Alleen zoeken op naam
**** DONE Alleen zoeken op stad
**** DONE Zoeken op naam en stad
*** DONE UC-203 - Details of student home
**** DONE Het opgevraagde huis bestaat niet
*** DONE UC-204 - Edit student home
**** DONE Gebruiker heeft geen toegang
**** DONE Eén of meer ingevoerde velden ontbreken
**** DONE Invalide postcode
**** DONE invalide telefoonnummer
**** DONE Het huis bestaat niet in het systeem
*** DONE UC-205 - Delete student home
**** DONE De gebruiker heeft geen toegang
**** DONE Het huis bestaat niet in het systeem

*** DONE UC-301 - Create meal in student home
**** DONE Verplicht veld ontbreekt
**** DONE Niet ingelogd
**** DONE Maaltijd succesvol toegevoegd
*** DONE UC-302 - Edit meal in student home
**** DONE Verplicht veld ontbreekt
**** DONE Niet ingelogd
**** DONE Niet de eigenaar van de data
**** DONE Maaltijd bestaat niet
**** DONE Maaltijd succesvol gewijzigd
*** DONE UC-303 - Get list of meals from student home
**** DONE Lijst van maaltijden geretourneerd
*** DONE UC-304 - Get details about meal from student home
**** DONE Maaltijd bestaat niet
**** DONE Details van maaltijd geretourneerd
*** DONE UC-305 - Delete meal from student home
**** DONE Niet ingelogd
**** DONE Niet de eigenaar van de data
**** DONE Maaltijd bestaat niet
**** DONE Maaltijd succesvol verwijderd

*** DONE UC-401 - Sign up for meal
**** DONE Niet ingelogd
**** DONE Maaltijd bestaat niet
**** DONE Succesvol aangemeld
*** DONE UC-402 - Sign out from meal
**** DONE Niet ingelogd
**** DONE Maaltijd bestaat niet
**** DONE Aanmelding bestaat niet
**** DONE Succesvol afgemeld

** Should have
*** TODO UC-403 - Get list of users who signed up for my meal
*** TODO UC-404 - Get list of details from user who signed up for my meal
