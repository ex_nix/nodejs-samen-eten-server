const express = require("express")
const mealController = require("../controllers/meal_controller")
const authController = require("../controllers/authentication_controller")
const router = express.Router()
const logger = require("tracer").console()

// middleware that is specific to this router
router.use(function (req, res, next) {
    logger.log("Time: ", Date.now().toLocaleString())
    next()
})

// BEHEER MAALTIJDEN
// TODO UC301
router.post("/studenthome/:homeId/meal",
    authController.validateToken,
    mealController.validateMeal,
    mealController.create)
// TODO UC302
router.put("/studenthome/:homeId/meal/:mealId",
    authController.validateToken,
    mealController.validateMeal,
    mealController.update)
// TODO UC303
router.get("/studenthome/:homeId/meal",
    mealController.getMealsByHomeId)
// TODO UC304
router.get("/studenthome/:homeId/meal/:mealId",
    mealController.getMealDetailsById)
// TODO UC305
router.delete("/studenthome/:homeId/meal/:mealId",
    authController.validateToken,
    mealController.delete)

module.exports = router
