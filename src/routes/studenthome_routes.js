const express = require("express")
const studentHomeController = require("../controllers/studenthome_controller")
const authController = require("../controllers/authentication_controller")
const router = express.Router()
const logger = require("tracer").console()

// middleware that is specific to this router
router.use(function (req, res, next) {
    logger.log("Time: ", Date.now().toLocaleString())
    next()
})

// BEHEER STUDENTENHUIS

// DONE UC201
router.post("/studenthome",
    authController.validateToken,
    studentHomeController.validateStudentHome,
    studentHomeController.create)
// DONE UC202
router.get("/studenthome",
    studentHomeController.getByNameAndCity)
// DONE UC203
router.get("/studenthome/:homeId",
    studentHomeController.getById)
// DONE UC204
router.put("/studenthome/:homeId",
    authController.validateToken,
    studentHomeController.validateStudentHome,
    studentHomeController.update)
// DONE UC205
router.delete("/studenthome/:homeId",
    authController.validateToken,
    studentHomeController.delete)
// TODO UC206
router.put("/studenthome/:homeId/user",
    authController.validateToken,
    studentHomeController.validateAddUserToHome,
    studentHomeController.addStudentToStudentHome)

// router.get("/studenthome", studentHomeController.getAll)

module.exports = router
