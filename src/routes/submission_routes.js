const express = require("express")
const submissionController = require("../controllers/submission_controller")
const authController = require("../controllers/authentication_controller")
const router = express.Router()
const logger = require("tracer").console()

// middleware that is specific to this router
router.use(function (req, res, next) {
    logger.log("Time: ", Date.now().toLocaleString())
    next()
})

// BEHEER AANMELDINGEN

// UC-401
router.post("/studenthome/:homeId/meal/:mealId/signup",
    authController.validateToken,
    submissionController.create)
// UC-402
router.put("/studenthome/:homeId/meal/:mealId/signoff",
    authController.validateToken,
    submissionController.delete)
// UC-403
router.get("/meal/:mealId/participants",
    authController.validateToken,
    submissionController.getListOfParticipants)
// UC-404
router.get("/meal/:mealId/participants/:participantId",
    authController.validateToken,
    submissionController.getDetailsAboutParticipant)

module.exports = router
